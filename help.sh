#!/bin/bash

echo ""
echo "Atribuição direta: Quando se usar o sinal de =. Exemplo:"
echo "Nome='Joanderson'"
echo ""
sleep 1

echo "Comando de atribuição: Quando queremos armazenar a saida do comando para uma váriavel. Exemplo:"
echo "caminho='$ () pwd'"
echo ""
sleep 1

echo "Argumentos de linhas de comando: Quando o usuário atribui os valores na linha de comando mesmo. Exemplo: "
echo "./diretorios.sh valor1 valor2"
echo ""
