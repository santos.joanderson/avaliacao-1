#!/bin/bash

dir1=$1
dir2=$2
dir3=$3

linhas=$(wc -l < "$dir1")
linhas2=$(wc -l < "$dir2")
linhas3=$(wc -l < "$dir3")

soma=$((linhas+linhas2+linhas3))

echo "> $soma"
