#!/bin/bash

echo -e "\e[1;33m Exibindo informações do hardaware \e[0m"
echo ""
sleep 2

echo "Exibindo informações detalhadas da CPU: "
lscpu
echo ""
sleep 2

echo "Exibindo informações sobre o dispositivos de armazenamento do sistema:"
lsblk
echo ""
sleep 2

echo "Exibindo informações sobre a tabela DMI:"
dmidecode
echo ""
sleep 2


echo "Exibindo informações sobre a memória do sistema:"
free
echo ""
sleep 2


echo "Exibindo informações sobre o uso da cpu em tempo real:"
top
echo ""
sleep 2

echo "Exibindo informações sobre os dispositivos USB conectados ao sistema:"
lsusb
echo ""
sleep 2

echo "Exibindo informações sobre todos os dispositivos PCI conectado:"
lspci
echo ""
sleep 1


echo "Exibindo informações sobre os procesos do sistema:"
ps
echo ""
sleep 1

exit 
