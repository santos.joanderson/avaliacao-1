#!/bin/bash

echo "1 - Não espere, ponha em prática!"
sleep 1
echo ""

echo "2 - Faça o seu melhor sempre!"
sleep 1
echo ""

echo "3 - Mesmo que pareça dificil, não pare!"
sleep 1
echo ""

echo "4 - Tenha coragem e enfrente o problema!"
sleep 1
echo ""

echo "5 - A felicidade não é algo pronto. Ela é feita das suas próprias ações."
sleep 1
echo ""

echo "6 - Desistir à primeira é para os fracos, tente sempre mais uma vez."
sleep 1
echo ""

echo "7 - Cada segundo é tempo para mudar tudo para sempre."
sleep 1
echo ""

echo "8 - Exise apenas um canto no universo que você pode ter certeza de apefeiçoar, que é você mesmo."
sleep 1
echo ""

echo "9 - Não há atalhos para nenhum destino onde vale a pena chegar!"
sleep 1
echo ""

echo "10 - A simplicidade é o ultimo grau de sofisticação."
sleep 1
echo ""
