#!/bin/bash

read -p "Digite o nome do diretorios 1: " dir1
read -p "Digite o nome do diretorio 2: " dir2
read -p "Digite o nome do diretorio 3: " dir3

txt_dir1=$(find "$dir1" -type f -name "*.txt" | wc -l)
png_dir1=$(find "$dir1" -type f -name "*.png" | wc -l)
doc_dir1=$(find "$dir1" -type f -name "*.doc" | wc -l)

txts_dir2=$(find "$dir2" -type f -name "*.txt" | wc -l)
pngs_dir2=$(find "$dir2" -type f -name "*.png" | wc -l)
docs_dir2=$(find "$dir2" -type f -name "*.doc" | wc -l)

txt_dir3=$(find "$dir3" -type f -name "*.txt" | wc -l)
png_dir3=$(find "$dir3" -type f -name "*.png" | wc -l)
doc_dir3=$(find "$dir3" -type f -name "*.doc" | wc -l)

echo "Diretorio 1: "
echo "Txt: $txt_dir1,Png: $png_dir1, Doc: $doc_dir1"
echo ""
sleep 1

echo "Diretorio 2: "
echo "Txt: $txts_dir2,Png: $pngs_dir2, Doc: $docs_dir2"
echo ""
sleep 1


echo "Diretorio 3: "
echo "Txt: $txt_dir3,Png: $png_dir3, Doc: $doc_dir3"
